Threat Types
===

- internal
  - unsafe pratices
    - external storage
      - storing sensative data
        - be lost off premisise
        - esaly be stolen
      - containing malisuse files
        - be found and plug into work equipment
        - modefiyed while off premises
    - untrusted webistes / files on the internet
    - upload files to internet / file shareing
    - poor BYOB polices
  - data theft
  - acidents
    - disclouser
    - data damage
- external
 - data theft
   - throw networking
   - equitment off site stolen
 - disruption of service
    - reasons
      - finiancial
        - can stop company making money
        - can worsen reputation increaseing that of competitors
      - political
        - other goverments vs other goverments
- Physical threats
  - modifcations of organisation equitment
    - keylogers
  - damage
    - malisouse
      - denial of service
    - acidental
      - floods
      - fire
      - people are just people
- social engineering
  - data extraction
      - banking details
      - personal records
  - sytems access
    - password resets
    - account actions eg ordering
- software
  - data extraction
    - send data off to a remote location
    - bypass access controls
  - monioring
    - keyloging
  - data ditruction
   - criptolockers / ransomware
    - can be used to extract cash asking for ransom to gain access to files / systems

Computer network based threats
===
- passive
  - wiretapping
    - Physical wiretap
      - pasive ethernet tap
        - modefiys cable to create a tap
        - can do full duplex intereptions but requiers two ethernet ports
        - can only do upto 100BASE-TX
        - the ethernet interfaces on reciveing computer resive all the packets both taped ends do
        - will include instruction and photos of it being done
      - pasive fiber tap
        - splice the cable to split the light beam in two
        - been used by goverments on undersea cables [^1]
        - is very expensive and needs specialest equitment
    - Local wiretap
      - wifi monitoring
        - instructions
          - run all as root
          - `airmon-ng start "interface name" "frquency"`
            - replace "interface name" with the wifi interface you want to sniff on
            - replace "frequency" with the freuqency you want to sniff on
          - start wireshark on mon0 interface
          - ![alt text](img/p1/wifi-sniff.png)
            - this image contains only boradcast frames and probe requests but could be data from a open ap
        - dose not requier anything physical modifcations to equipment
        - can be protected against by using wifi encription
          - should be WPA2
          - wep and wpa can be cracked
          - will include instruction and photos of it being done
        - WPA2 will not help in a enterpise settings
          - anyone who knows the password can intercept the key exchange and intercept all traffic
          - WPA2 enterpise should be used as it provides per user credentials
  - port scanning
    - tradional
      - sending a sync packet to the host on a port and checking if a syn ack packet is recived
      - can be detected if lots of ports on a host or network are being scanned
      - attacker can be identfiyed as the syn ack is sent back to them.
      - instructions
        - many programs out there that do it
        - `nmap "target ip"`
          - replace "target ip" with target very simple
    - idle scanning [^2]
      - similar to port scanning but the attacker is harder to be identfiyed
      - requirements
        - a computer on the network that dose not have randomised packet IDs this will be used as the vombie
        - the ability to spoof source address of the attacker to be that of t he vombie
      - steps
        1. query the vombie for the packet ID
        2. send syn packet to target with spoofed source address of the vombie
        3. the target will repond with a syn ack to the vombie if the port is open
        4. the vombie will respond with a reset packet to the syn ack
        5. read the packet ID from the vombie if it has increased then a reset packet has been sent the port must be open.
- active
  - man in the middle
    - attacker places self in two comunicating computers
    - they are able to lissening on and modefiy comunications betwen them
    - types
      - arp posioning
        - in networks packets are comonly sent throw switchers
        - switiches switch packets
        - will only send them out on ports they know the mac address is on
        - attacker emits lots of packets to switch with spoofed mac of a computer the target is comunicating with or the gateway
        - switch will update is lookup tables to set that mac being going out of the attackers switch ports
        - attacker will recive traffic bound for computer the target is comunicating to
  - denial of service
    - smurf
    - DNS
    - NTP

information security
===

- confidentiality
- integrity
- availability

The law
===
- Data Protection Act 1998 [^3]
- Computer Misuse Act 1990 [^4]
- Copyright, Designs and Patents Act 1988
- Telecommunications (Lawful Business Practice) (Interception of Communications) Regulations 2000
- Fraud Act 2006
- Requlators
  - FCA [^5] (personal experence at work we are soon to be regulated by them)
    - iso certifcation

[^1]: [http://www.defensetech.org/2005/02/21/jimmy-carter-super-spy/]
[^2]: [https://en.wikipedia.org/wiki/Idle_scan]
[^3]: [http://www.legislation.gov.uk/ukpga/1998/29/contents]
[^4]: [http://www.legislation.gov.uk/ukpga/1990/18/contents]
[^5]: [https://www.fca.org.uk/]
